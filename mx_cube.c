void upside(int horizontal_side, int vertical_side, int diagonal_side);
void front(int horizontal_side, int vertical_side, int diagonal_side);
void mx_printchar(char c);

static void print_line(int n,
                       char c) {
    for (int i = 0; i < n; i++) {
        mx_printchar(c);
    }
}
static void print_line_with_on_edges(int n,
                                     char c,
                                     char edge) {
    mx_printchar(edge);
    print_line(n, c);
    mx_printchar(edge);
}
void mx_cube(int n){
    int vertical_side = n;
    int horizontal_side = n*2;
    int diagonal_side = n/2;

    if(n<2){
        return;
    }

    upside(horizontal_side, vertical_side, diagonal_side);
    front(horizontal_side, vertical_side, diagonal_side);
}
void upside(int horizontal_side,
            int vertical_side,
            int diagonal_side){
    print_line(diagonal_side + 1, ' ');
    print_line_with_on_edges(horizontal_side, '-', '+');
    mx_printchar('\n');
    for (int i = 0; i < diagonal_side; ++i) {
        print_line(diagonal_side-i, ' ');
        print_line_with_on_edges(horizontal_side, ' ', '/');
        print_line(i, ' ');
        mx_printchar('|');
        mx_printchar('\n');
    }
    print_line_with_on_edges(horizontal_side, '-', '+');
    print_line(diagonal_side, ' ');
    mx_printchar('|');
    mx_printchar('\n');
}
void front(int horizontal_side,
           int vertical_side,
           int diagonal_side) {
    for (int i = 0; i < vertical_side / 2; ++i) {
        print_line_with_on_edges(horizontal_side, ' ', '|');
        print_line(diagonal_side, ' ');
        mx_printchar('|');
        mx_printchar('\n');
    }
    print_line_with_on_edges(horizontal_side, ' ', '|');
    print_line(diagonal_side, ' ');
    mx_printchar('+');
    mx_printchar('\n');
    for (int i = 0; i < vertical_side/2; ++i) {
        print_line_with_on_edges(horizontal_side, ' ', '|');
        print_line(diagonal_side-i-1, ' ');
        mx_printchar('/');
        mx_printchar('\n');
    }
    print_line_with_on_edges(horizontal_side, '-', '+');
    mx_printchar('\n');
}


