void mx_printchar(char c);
void mx_print_left(int left_side);
void mx_print_mid(int left_side,
                  int mid_counter,
                  int n,
                  int right_counter);
void mx_print_right(int mid_counter,
                    int left_side,
                    int *right_counter,
                    int n);

void mx_pyramid(int n) {
    int left_side = n - 1;
    int right_counter = n - 1;
    int mid_counter = n - 2;

    if (n % 2 != 0
    || n < 1) {
        return;
    }
    n--;
    while (left_side >= 0) {
        mx_print_left(left_side);
        mx_print_mid(left_side, mid_counter, n, right_counter);
        mid_counter++;
        mx_print_right(mid_counter, left_side, &right_counter, n);
        left_side--;
        mx_printchar('\n');
    }
}
void mx_print_left(int left_side) {
    for (int j = left_side; j != 0; j--) {
        mx_printchar(' ');
    }

    mx_printchar('/');
}
void mx_print_mid(int left_side,
                  int mid_counter,
                  int n,
                  int right_counter) {
    if (left_side != 0) {
        for (int j = left_side; j < mid_counter; j++) {
            mx_printchar(' ');
        }
    }
    else {
        for (int j = left_side; j < mid_counter; j++) {
            mx_printchar('_');
        }
    }

    if (right_counter > n) {
        mx_printchar('\\');
    }
}
void mx_print_right(int mid_counter,
                    int left_side,
                    int *right_counter,
                    int n) {
    for (int j = mid_counter; j < *right_counter; j++) {
        mx_printchar(' ');
    }

    if (left_side > n / 2) {
        if (left_side - 1 == n / 2) {
            (*right_counter)++;
        }
        else {
            *right_counter += 2;
        }

        mx_printchar('\\');
    }
    else {
        mx_printchar('|');
    }
}
